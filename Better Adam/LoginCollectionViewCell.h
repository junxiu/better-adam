//
//  LoginCollectionViewCell.h
//  Better Adam
//
//  Created by Jun Xiu Chan on 9/23/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXButton.h"

@protocol LoginCollectionViewCellDelegate <NSObject>

- (void)didEnterEmail:(NSString *)email andPassword:(NSString *)password;

@end

@interface LoginCollectionViewCell : UICollectionViewCell

@property id <LoginCollectionViewCellDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *separatorView;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@end
