//
//  OnboardViewController.h
//  Better Adam
//
//  Created by Jun on 18/9/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXButton.h"
#import "JCRBlurView.h"

@interface OnboardViewController : UIViewController

@property (strong, nonatomic) IBOutlet UICollectionView *onboardingCollectionView;
@property (weak, nonatomic) IBOutlet JXButton *loginButton;
@property (strong, nonatomic) IBOutlet UIPageControl *onboardingPageControl;
@property (strong, nonatomic) IBOutlet UIImageView *onboardingWallpaperImageView;
@property (strong, nonatomic) IBOutlet UIImageView *onboardingBlurWallpaperImageView;
@property (strong, nonatomic) IBOutlet JXButton *loginViaFacebookButton;
- (IBAction)skipToLogin:(id)sender;
- (IBAction)loginWithFacebook:(id)sender;
@end
