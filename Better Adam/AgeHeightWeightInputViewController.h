//
//  AgeHeightWeightInputViewController.h
//  Better Adam
//
//  Created by Jun on 30/9/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXButton.h"

@interface AgeHeightWeightInputViewController : UIViewController
@property (strong, nonatomic) IBOutlet JXButton *nextButton;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundWallpaperImageView;
@property (strong, nonatomic) IBOutlet UILabel *stageDescriptionLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *inputCollectionView;
@property (strong, nonatomic) IBOutlet UILabel *unitLabel;
@end
