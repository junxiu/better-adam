//
//  JXButton.m
//  ButtonWithBorder
//
//  Created by Jun Xiu Chan on 2/21/14.
//  Copyright (c) 2014 JX. All rights reserved.
//

#import "JXButton.h"

@interface JXButton()
@end
@implementation JXButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)awakeFromNib{
    [self setup];
}

- (void)setup{
    
    //default
    
    [self setClipsToBounds:YES];
    
    [self setAnimateDuration:0.05f];
    
    [self addTarget:self action:@selector(touchDown) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(touchExit) forControlEvents:(UIControlEventTouchUpInside|UIControlEventTouchUpOutside|UIControlEventTouchCancel|UIControlEventTouchDragExit)];
}

- (void)setCornerRadius:(CGFloat)cornerRadius setBorderWidth:(CGFloat)borderWidth setBorderColor:(UIColor*)borderColor{
    self.borderColor = borderColor;
    self.borderWidth = borderWidth;
    [self.layer setCornerRadius:cornerRadius];
    [self.layer setBorderWidth:borderWidth];
    [self.layer setBorderColor:borderColor.CGColor];
}

- (void)touchDown{
    self.defaultBackgroundColor = self.backgroundColor;
    [self changeButtonBackgroundColorTo:self.touchDownButtonBackgroundColor borderColorTo:self.touchDownButtonBorderColor withDuration:self.animateDuration];
}
- (void)touchExit{
    [self changeButtonBackgroundColorTo:self.defaultBackgroundColor borderColorTo:self.borderColor withDuration:self.animateDuration * 7];
}


- (void)changeButtonBackgroundColorTo:(UIColor* )backgroundColor borderColorTo:(UIColor *)borderColor withDuration:(CGFloat)duration{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [self setBackgroundColor:backgroundColor];
    self.layer.borderColor= borderColor.CGColor;
    [UIView commitAnimations];
    
    CABasicAnimation* borderAnimation = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    [borderAnimation setFromValue:self.borderColor];
    [borderAnimation setToValue:borderColor];
    [borderAnimation setAutoreverses:NO];
    [borderAnimation setDuration:duration];
    
    [self.layer addAnimation:borderAnimation forKey:@"animateColor"];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
