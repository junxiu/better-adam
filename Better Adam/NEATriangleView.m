//
//  NEATriangleView.m
//  myENV
//
//  Created by Jun Xiu Chan on 6/14/14.
//  Copyright (c) 2014 National Environment Agency. All rights reserved.
//

#import "NEATriangleView.h"

@interface NEATriangleView()
@property (nonatomic) NSInteger direction;

@end
@implementation NEATriangleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame direction:(TriangleDirection)direction{
    self = [super initWithFrame:frame];
    if (self) {
        _direction = direction;
    }
    return self;
}

- (void)drawRect:(CGRect)rect{
    if (!_triangleViewColor) {
        _triangleViewColor = [UIColor blueColor];
    }
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextBeginPath(ctx);
    if (_direction == kTop) {
        CGContextMoveToPoint   (ctx, CGRectGetMinX(rect), CGRectGetMaxY(rect));  // bottom left
        CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMaxY(rect));  // bottom right
        CGContextAddLineToPoint(ctx, CGRectGetMidX(rect), CGRectGetMinY(rect));  // mid
    }
    else if (_direction == kBottom) {
        CGContextMoveToPoint   (ctx, CGRectGetMinX(rect), CGRectGetMinY(rect));  // top left
        CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMinY(rect));  // mid right
        CGContextAddLineToPoint(ctx, CGRectGetMidX(rect), CGRectGetMaxY(rect));  // bottom left
    }
    
    CGContextClosePath(ctx);
    
    const CGFloat* components = CGColorGetComponents(_triangleViewColor.CGColor);
    
    CGContextSetRGBFillColor(ctx, components[0], components[1], components[2], CGColorGetAlpha(_triangleViewColor.CGColor));

    if ([_triangleViewColor isEqual:[UIColor whiteColor]]) {
        CGContextSetRGBFillColor(ctx, 1, 1, 1, 1);
    }
    
    CGContextFillPath(ctx);
}
@end
