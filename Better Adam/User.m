//
//  User.m
//  Better Adam
//
//  Created by Jun Xiu Chan on 9/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "User.h"

@implementation User

+ (BOOL)validateUserWithLoginInfo:(NSDictionary *)infoDict {
    // network validation
    
    // if user is in database,
    // check if password is correct
    
    // if user is not in database,
    // prompt user for registration
    
    // if password is incorrect
    // prompt user about it.
    
    // if user exist and password is correct
    // get full user information from server
    // then store it in user default
    
#warning for now, dumb dict will be saved in userdefault
    
    NSMutableDictionary *dumbDict = [NSMutableDictionary dictionary];
    
    // respones dict should never return user email address and password.
    
    [dumbDict setObject:@"Jun Xiu" forKey:@"name"];
    [dumbDict setObject:@"70" forKey:@"weight"];
    [dumbDict setObject:@"1.85m" forKey:@"height"];
    [dumbDict setObject:@"22" forKey:@"age"];
    
    [[NSUserDefaults standardUserDefaults] setObject:dumbDict forKey:@"userInfo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return YES;
}

+ (BOOL)validateFacebookUserWithID:(NSString *)facebookID {
    return YES;
}

@end
