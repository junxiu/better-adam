//
//  VitalInput.h
//  Better Adam
//
//  Created by Jun on 29/9/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, VitalSetupStage) {
    kAgeWeightHeight,
    kJeans,
    kSmoke,
    kExerciseRegularity,
};

@interface VitalInput : NSObject

+ (id)sharedInstance;

- (NSDictionary *)vitalDictionaryForStage:(VitalSetupStage)stage;
@end
