//
//  VitalInput.m
//  Better Adam
//
//  Created by Jun on 29/9/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "VitalInput.h"

@implementation VitalInput

+ (id)sharedInstance {
    static id sharedAPI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAPI = [[self alloc] init];
    });
    return sharedAPI;
}

- (NSArray *)vitalStateSequenceArray {
    NSArray *responseArray = @[[NSNumber numberWithInt:kAgeWeightHeight],
                               [NSNumber numberWithInt:kJeans],
                               [NSNumber numberWithInt:kSmoke],
                               [NSNumber numberWithInt:kExerciseRegularity]];
    return responseArray;
}

- (NSDictionary *)vitalDictionaryForStage:(VitalSetupStage)stage {
    NSMutableDictionary *responseDictionary = [NSMutableDictionary dictionary];
    NSString *description;
    NSString *identifier;
    
    switch (stage) {
        case kAgeWeightHeight:
            description = @"";
            identifier = @"InputScalarSliderCollectionViewCell";
            break;
            
        case kJeans:
            identifier = @"InputScalarSliderCollectionViewCell";
            break;
            
        case kSmoke:
            identifier = @"InputSegmentedControlCollectionViewCell";
            break;
            
        case kExerciseRegularity:
            identifier = @"InputScalarSliderCollectionViewCell";
            break;
            
        default:
            break;
    }
    
    [responseDictionary setObject:description forKey:@"description"];
    [responseDictionary setObject:identifier forKey:@"identifier"];
    
    return responseDictionary;
}

@end
