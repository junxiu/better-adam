//
//  InputScalarSliderCollectionViewCell.h
//  Better Adam
//
//  Created by Jun on 29/9/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputScalarSliderCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@end
