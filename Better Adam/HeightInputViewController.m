//
//  HeightInputViewController.m
//  Better Adam
//
//  Created by Jun Xiu Chan on 10/2/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "HeightInputViewController.h"
#import "DashboardViewController.h"

#import "InputScalarSliderCollectionViewCell.h"

#import "UIImage+ImageEffects.h"
#import "UIView+Frame.h"

#import "VitalInput.h"

@interface HeightInputViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>
@property VitalSetupStage currentStage;
@property NSInteger initialValue;
@property NSInteger skipValue;
@end

@implementation HeightInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self additionalSetup];
}

- (void)additionalSetup {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [_inputCollectionView setContentInset:UIEdgeInsetsMake(0, -2350, 0, 145)];
    _initialValue = 100;
    [self setUnitValue:0];
    _triangleView = [[NEATriangleView alloc] initWithFrame:CGRectMake(156, 170, 11, 8) direction:kBottom];
    _triangleView.backgroundColor = [UIColor clearColor];
    _triangleView.triangleViewColor = [UIColor colorWithRed:1.000 green:0.804 blue:0.184 alpha:1];
    [self.view addSubview:_triangleView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

- (void)setUnitValue:(NSInteger)value {
    NSString *desc = @"HEIGHT";
    NSString *unit = @"cm";
    
    _unitLabel.text = [NSString stringWithFormat:@"%@ %li%@",desc,(long)value + _initialValue ,unit];
    NSMutableAttributedString * attributedString= [[NSMutableAttributedString alloc] initWithString:_unitLabel.text];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Gotham-Medium" size:8] range:NSMakeRange(0,desc.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GothamBold" size:15] range:NSMakeRange(desc.length, _unitLabel.text.length - desc.length)];
    _unitLabel.attributedText = attributedString;
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat maxValue = 200; //age
    CGFloat size = 30; //px between cell
    CGFloat totalContentWidth = scrollView.contentSize.width + scrollView.contentInset.left - scrollView.contentInset.right - size;
    CGFloat currentOffset = scrollView.contentOffset.x + scrollView.contentInset.left;
    CGFloat value = currentOffset / totalContentWidth * maxValue;
    if (value <= maxValue && value >= 0) {
        CGFloat roundUp = 0.5f;
        [self setUnitValue:value + roundUp];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 301;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(25, self.view.frame.size.height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item % 5 == 0) {
        UICollectionViewCell *longCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"longHandCarouselCollectionViewCell" forIndexPath:indexPath];
        
        if (indexPath.item > 99) {
            for (UIView *view in longCell.contentView.subviews) {
                [view setBackgroundColor:[UIColor colorWithRed:1.000 green:0.804 blue:0.184 alpha:1]];
            }
            [(UILabel *)[longCell.contentView viewWithTag:1] setText:[NSString stringWithFormat:@"%li",(indexPath.item / 1 + _initialValue) - 100]];
            [(UILabel *)[longCell.contentView viewWithTag:1] setBackgroundColor:[UIColor clearColor]];
            
        }
        else {
            for (UIView *view in longCell.contentView.subviews) {
                [view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
            }
            [(UILabel *)[longCell.contentView viewWithTag:1] setText:@""];
            [(UILabel *)[longCell.contentView viewWithTag:1] setBackgroundColor:[UIColor clearColor]];
        }
        
        return longCell;
    }
    else {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"shortHandCarouselCollectionViewCell" forIndexPath:indexPath];
        
        if (indexPath.item < 100) {
            for (UIView *view in cell.contentView.subviews) {
                [view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
            }
        }
        else {
            for (UIView *view in cell.contentView.subviews) {
                [view setBackgroundColor:[UIColor colorWithRed:1.000 green:0.804 blue:0.184 alpha:1]];
            }
        }
        
        return cell;
    }
}

@end
