//
//  OnboardViewController.m
//  Better Adam
//
//  Created by Jun on 18/9/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import "OnboardViewController.h"
#import "LoginCollectionViewCell.h"
#import "UIView+Frame.h"
#import "UIImage+ImageEffects.h"
#import "User.h"

@interface OnboardViewController () <UICollectionViewDataSource, UICollectionViewDelegate, LoginCollectionViewCellDelegate>
@property NSMutableDictionary *loginInfoDict;
@property NSString *email;
@property NSString *password;
@property NSString *facebookID;
@end

@implementation OnboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self additionalSetup];
}

- (void)additionalSetup {
    [_loginButton setCornerRadius:3
                   setBorderWidth:1
                   setBorderColor:_loginButton.backgroundColor];
    
    [_loginButton setTouchDownButtonBorderColor:_loginButton.backgroundColor];
    [_loginButton setTouchDownButtonBackgroundColor:_loginButton.backgroundColor];
    
    [_loginViaFacebookButton setCornerRadius:3
                   setBorderWidth:1
                   setBorderColor:_loginViaFacebookButton.backgroundColor];
    
    [_loginViaFacebookButton setTouchDownButtonBorderColor:_loginViaFacebookButton.backgroundColor];
    [_loginViaFacebookButton setTouchDownButtonBackgroundColor:_loginViaFacebookButton.backgroundColor];
    
    _loginViaFacebookButton.titleLabel.numberOfLines = 0;
    
    _onboardingBlurWallpaperImageView.image = [[UIImage imageNamed:@"onboardingWallpaper.jpg"] applyBlurWithRadius:7 tintColor:[UIColor clearColor] saturationDeltaFactor:1 maskImage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - LoginCollectionViewCellDelegate

- (void)didEnterEmail:(NSString *)email andPassword:(NSString *)password{
    _email = email;
    _password = password;
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

#warning placeholder content

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == 3) {
        LoginCollectionViewCell *loginCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LoginCollectionViewCell" forIndexPath:indexPath];
        loginCell.delegate = self;
        return loginCell;
    }
    else {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self switchPage];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat threshold = _onboardingCollectionView.contentSize.width - (self.view.frame.size.width * 2);
    _onboardingBlurWallpaperImageView.alpha = (scrollView.contentOffset.x - threshold) * 0.0025;
    [_onboardingWallpaperImageView setFrameOriginX:-scrollView.contentOffset.x * 0.25f];
    [_onboardingBlurWallpaperImageView setFrameOriginX:-scrollView.contentOffset.x * 0.25f];
    
    [self switchPage];
}

#pragma mark - Methods

- (void)switchPage {
    [self.view endEditing:YES];
    CGFloat pageWidth = _onboardingCollectionView.frame.size.width;
    _onboardingPageControl.currentPage = _onboardingCollectionView.contentOffset.x / pageWidth;
    if (_onboardingPageControl.currentPage != _onboardingPageControl.numberOfPages - 1) {
        [_loginButton setTitle:@"Skip to login" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.45f
                              delay:0
             usingSpringWithDamping:1
              initialSpringVelocity:0.55f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [_loginButton setFrameWidth:280];
                             [_loginViaFacebookButton setFrameOriginX:self.view.frame.size.width];
                         } completion:^(BOOL finished) {
                             //
                         }];
    }
    else {
        [_loginButton setTitle:@"Login" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.45f
                              delay:0
             usingSpringWithDamping:1
              initialSpringVelocity:0.55f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [_loginButton setFrameWidth:130];
                             [_loginViaFacebookButton setFrameWidth:130];
                             [_loginViaFacebookButton setFrameOriginX:CGRectGetMaxX(_loginButton.frame) + 20];
                         } completion:^(BOOL finished) {
                             //
                         }];
    }
}

- (IBAction)skipToLogin:(id)sender {
    if ([_loginButton.titleLabel.text isEqualToString:@"Login"]) {
        [_loginInfoDict setObject:_email forKey:@"email"];
        [_loginInfoDict setObject:_password forKey:@"password"];
        
        if ([User validateUserWithLoginInfo:_loginInfoDict]) {
            [[[UIAlertView alloc] initWithTitle:@"Login Successful" message:@"Welcome to Better Adam" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            [self loginSuccessfuly];
        }
    }
    else {
        [self.onboardingCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        [_loginButton setTitle:@"Login" forState:UIControlStateNormal];
    }
}

- (IBAction)loginWithFacebook:(id)sender {
    [FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:YES completionHandler: ^(FBSession *session, FBSessionState state, NSError *error) {
        [self sessionStateChanged:session state:state error:error];
    }];
}

- (void)loginSuccessfuly {
    UINavigationController *vitalSetupNavigationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"vitalSetupNavigationViewController"];
    [self presentViewController:vitalSetupNavigationViewController animated:YES completion:nil];
}

#pragma mark - Facebook

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error {
    if (!error) {
        switch (state) {
            case FBSessionStateOpen:
            {
                NSLog(@"done");
                [self populateUserDetails];
            }
                break;
            case FBSessionStateClosedLoginFailed:
            {
                [FBSession.activeSession closeAndClearTokenInformation];
            }
                break;
            default:
                break;
        }
    }
}

- (void)populateUserDetails {
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
            if (!error) {
                _facebookID = [NSString stringWithFormat:@"%@",[user objectForKey:@"id"]];
                if ([User validateFacebookUserWithID:_facebookID]) {
                    [[[UIAlertView alloc] initWithTitle:@"Login Successful Via Facebook" message:@"Welcome to Better Adam" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
                }
                [self loginSuccessfuly];
            }
        }];
    }
}

@end
