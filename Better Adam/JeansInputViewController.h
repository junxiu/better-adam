//
//  JeansInputViewController.h
//  Better Adam
//
//  Created by Jun Xiu Chan on 10/2/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXButton.h"
#import "NEATriangleView.h"


@interface JeansInputViewController : UIViewController
@property (strong, nonatomic) IBOutlet JXButton *nextButton;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundWallpaperImageView;
@property (strong, nonatomic) IBOutlet UILabel *stageDescriptionLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *inputCollectionView;
@property (strong, nonatomic) IBOutlet UILabel *unitLabel;
@property (strong, nonatomic) NEATriangleView *triangleView;

@end
