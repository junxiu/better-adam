//
//  User.h
//  Better Adam
//
//  Created by Jun Xiu Chan on 9/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

+ (BOOL)validateUserWithLoginInfo:(NSDictionary *)infoDict;
+ (BOOL)validateFacebookUserWithID:(NSString *)facebookID;

@end
