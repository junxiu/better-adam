//
//  VitalSetupViewController.m
//  Better Adam
//
//  Created by Jun Xiu Chan on 9/23/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "AgeHeightWeightInputViewController.h"

#import "InputScalarSliderCollectionViewCell.h"

#import "UIImage+ImageEffects.h"
#import "UIView+Frame.h"

#import "VitalInput.h"

@interface AgeHeightWeightInputViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
@property VitalSetupStage currentStage;
@property NSInteger initialValue;
@property NSInteger skipValue;
@end

@implementation AgeHeightWeightInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self additionalSetup];
}

- (void)additionalSetup {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 200;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(30, self.view.frame.size.height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item % 5 == 0) {
        UICollectionViewCell *loginCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"longHandCarouselCollectionViewCell" forIndexPath:indexPath];
        [(UILabel *)[loginCell.contentView viewWithTag:1] setText:[NSString stringWithFormat:@"%li",indexPath.item / 5 + _initialValue]];
        return loginCell;
    }
    else {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"shortHandCarouselCollectionViewCell" forIndexPath:indexPath];
        return cell;
    }
}


@end
