//
//  JXButton.h
//  ButtonWithBorder
//
//  Created by Jun Xiu Chan on 2/21/14.
//  Copyright (c) 2014 JX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JXButton : UIButton

@property (nonatomic) UIColor *touchDownButtonBackgroundColor;
@property (nonatomic) UIColor *touchDownButtonBorderColor;
@property (nonatomic) CGFloat animateDuration;

@property (nonatomic) UIColor *defaultBackgroundColor;
@property (nonatomic) UIColor *borderColor;
@property (nonatomic) CGFloat borderWidth;

- (void)setCornerRadius:(CGFloat)cornerRadius setBorderWidth:(CGFloat)borderWidth setBorderColor:(UIColor*)borderColor;
@end
