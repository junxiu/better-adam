//
//  LoginCollectionViewCell.m
//  Better Adam
//
//  Created by Jun Xiu Chan on 9/23/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "LoginCollectionViewCell.h"

@implementation LoginCollectionViewCell

- (void)awakeFromNib {
    _separatorView.frame = CGRectMake(_separatorView.frame.origin.x, _separatorView.frame.origin.y, _separatorView.frame.size.width, 0.5f);
    
    UIColor *color = [UIColor colorWithWhite:0 alpha:0.3];
    
    _emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    _passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:_emailTextField]) {
        [_passwordTextField becomeFirstResponder];
        return NO;
    }
    else {
        [_passwordTextField resignFirstResponder];
    }
    [self.delegate didEnterEmail:_emailTextField.text andPassword:_passwordTextField.text];
    return YES;
}
@end
