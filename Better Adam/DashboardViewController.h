//
//  DashboardViewController.h
//  Better Adam
//
//  Created by Jun Xiu Chan on 9/26/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAMGradientView.h"
@interface DashboardViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (strong, nonatomic) IBOutlet UIImageView *userBackdropProfileBlurImageView;
@property (strong, nonatomic) IBOutlet SAMGradientView *userNameContentView;
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@end
