//
//  main.m
//  Better Adam
//
//  Created by Jun on 18/9/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
