//
//  SmokeInputViewController.m
//  Better Adam
//
//  Created by Jun on 29/9/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "SmokeInputViewController.h"

#import "UIView+Frame.h"

@interface SmokeInputViewController ()

@end

@implementation SmokeInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_segmentedControl setFrameHeight:100];
    
    UIFont *font = [UIFont fontWithName:@"GothamBook" size:15];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    [_segmentedControl setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentedControlValueChanged:(id)sender {
}
@end
