//
//  NEATriangleView.h
//  myENV
//
//  Created by Jun Xiu Chan on 6/14/14.
//  Copyright (c) 2014 National Environment Agency. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, TriangleDirection) {
    kTop,
    kRight,
    kBottom,
    kLeft,
};

@interface NEATriangleView : UIView

- (instancetype)initWithFrame:(CGRect)frame direction:(TriangleDirection)direction;
@property (nonatomic) UIColor *triangleViewColor;
@end
